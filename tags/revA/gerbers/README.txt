Boston University P/N G2-TDC-MB Rev B  2015-04-20

4 Layers
Final thickness 0.063 in
Surface finish ENIG

White silkscreen both sides
Green soldermask both sides

G2-TDC-MB-B_Mask.pho		Bottom solder mask
G2-TDC-MB-B_Paste.pho		Bottom solder paste
G2-TDC-MB-B_SilkS.pho		Bottom silkscreen
G2-TDC-MB.drl			NC drill data
G2-TDC-MB-drl_map.pho		NC drill map
G2-TDC-MB-Fab_Dwg.pho		Fabrication drawing
G2-TDC-MB-F_Mask.pho		Top solder mask
G2-TDC-MB-F_Paste.pho		Top solder paste
G2-TDC-MB-F_SilkS.pho		Top silkscreen
G2-TDC-MB-layer_1.pho		Layer 1 0.5 oz (signal)
G2-TDC-MB-layer_2.pho		Layer 2 0.5 oz (power, positive)
G2-TDC-MB-layer_3.pho		Layer 3 0.5 oz (power, positive)
G2-TDC-MB-layer_4.pho		Layer 4 0.5 oz (signal)
G2-TDC-MB-NPTH.drl		NC drill data non-plated holes
G2-TDC-MB-NPTH-drl_map.pho	NC drill map non-plated holes

