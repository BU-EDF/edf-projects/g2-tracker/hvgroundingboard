#!/bin/bash
#
# make zip file and template README for this project
# generate gerbers with "use proper filename extensions" UNCHECKED
# so all the gerber file names end in ".pho"
#
base="G2-HV-GND"
orig="HVGroundingBoard"
mv ${orig}-B_Cu.pho ${base}-layer_2.pho
mv ${orig}-B_Mask.pho ${base}-B_Mask.pho
mv ${orig}-B_SilkS.pho ${base}-B_SilkS.pho
mv ${orig}-Dwgs_User.pho ${base}-Fab_Dwg.pho
mv ${orig}.drl ${base}.drl
mv ${orig}-drl_map.pho ${base}-drl_map.pho
mv ${orig}-F_Cu.pho ${base}-layer_1.pho
mv ${orig}-F_Mask.pho ${base}-F_Mask.pho
mv ${orig}-F_SilkS.pho ${base}-F_SilkS.pho
mv ${orig}-Edge_Cuts.pho ${base}-Edge_Cuts.pho

