PCBNEW-LibModule-V1  Mon 20 Apr 2015 09:21:17 PM EDT
# encoding utf-8
Units mm
$INDEX
805
mountingscrews
pingroundconn
shvmount
$EndINDEX
$MODULE 805
Po 0 0 0 15 5519952C 00000000 ~~
Li 805
Sc 0
AR 
Op 0 0 0
T0 0 -1.8 1 1 0 0.15 N V 21 N "805"
T1 0 2 1 1 0 0.15 N V 21 N "VAL**"
$PAD
Sh "1" R 0.7 1.3 0 0 0
Dr 0 0 0
At SMD N 00880001
Ne 0 ""
Po -0.95 0
$EndPAD
$PAD
Sh "2" R 0.7 1.3 0 0 0
Dr 0 0 0
At SMD N 00880001
Ne 0 ""
Po 0.95 0
$EndPAD
$EndMODULE 805
$MODULE mountingscrews
Po 0 0 0 15 55354D67 00000000 ~~
Li mountingscrews
Sc 0
AR 
Op 0 0 0
T0 2.1 -3 1 1 0 0.15 N I 21 N "mountingscrews"
T1 4.6 2.4 1 1 0 0.15 N I 21 N "VAL**"
$PAD
Sh "1" C 3.9 3.9 0 0 0
Dr 3.73 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 0
.LocalClearance 1
$EndPAD
$EndMODULE mountingscrews
$MODULE pingroundconn
Po 0 0 0 15 550C7A7A 00000000 ~~
Li pingroundconn
Sc 0
AR 
Op 0 0 0
T0 0 -6.2 1 1 0 0.15 N V 21 N "pingroundconn"
T1 0 5.6 1 1 0 0.15 N V 21 N "VAL**"
$PAD
Sh "1" R 1 3.15 0 0 0
Dr 0 0 0
At SMD N 00880001
Ne 0 ""
Po 0 2.525
$EndPAD
$PAD
Sh "2" R 1 3.15 0 0 0
Dr 0 0 0
At SMD N 00880001
Ne 0 ""
Po 0 -2.525
$EndPAD
$EndMODULE pingroundconn
$MODULE shvmount
Po 0 0 0 15 5535A607 00000000 ~~
Li shvmount
Sc 0
AR /550C5DB0
Op 0 0 0
.LocalClearance 4
.ZoneConnection 2
T0 -0.15 7.93 1 1 0 0.15 N I 21 N "P1"
T1 8.75 -7.57 1 1 0 0.15 N I 21 N "SHV"
DS 4.6 -8 9.3 0 0.15 25
DS -4.6 8 -9.3 0 0.15 25
DS -9.3 0 -4.6 -8 0.15 25
DS 9.3 0 4.7 8 0.15 25
DS -4.6 8 4.64 8 0.15 25
DS -4.6 -8 4.64 -8 0.15 25
$PAD
Sh "1" C 15.98 15.98 0 0 0
Dr 12.95 0 0
At STD N 00E0FFFF
Ne 1 "N-000004"
Po 0 0
.LocalClearance 2.485
.ZoneConnection 2
$EndPAD
$EndMODULE shvmount
$EndLIBRARY
